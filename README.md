# DEPRECATED, USE SRRG2_ORAZIO

# srrg_orazio_ros

This package contains a ros node for the orazio robot

## Setting up
Just copy the node in your ros_package_path and compile.

Requires `srrg_core` and `srrg_orazio_core`.

**Parameters**:
* `_serial_device`:     (/dev/ttyUSB0) the serial port
* `_odom_topic`:        (/odom) the odometry topic 
* `_command_vel_topic`: (/cmd_vel) the cmd_vel topic
* `_joint_state_topic`: (/joint_state) joint state topic for use with calibration tools
* `_ticks_topic`:       (/ticks) raw encoder ticks message
* `_odom_frame_id`:     (/odom) frame id for the odometry message

That's all.

When launching the node, by default it does not publish transforms!!!
See `srrg_core/srrg_state_publisher_node` to publish the entire transform
tree upon receiving an odometry message.

## Authors

* **Giorgio Grisetti** 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## Typical setup for a laser orazio controlled with a joystick (ROS)
Required packages: 
* srrg_boss
* srrg_cmake_modules
* srrg_core
* srrg_orazio_core
* thin_drivers (thin_hokuyo, thin_joystick_teleop)

Example launch files:
* `robot_laser.launch` (absolute paths have to be adjusted accordingly)

## License

BSD 2.0